'use strict';

const Hapi = require('hapi');
const Boom = require('boom');
const Pack = require('./package');
const HapiSwagger = require('hapi-swagger');

// Create a server with a host and port
const server = Hapi.server({
    host: '192.168.43.114',
    port: 8000,
    routes: {
        cors: true
    }
});

const swaggerOptions = {
    info: {
        title: 'Check List API Documentation',
        version: Pack.version,
    },
};

const dbOpts = {
    url: 'mongodb://localhost:27017/checklistdb',
    settings: {
        poolSize: 10
    },
    decorate: true
};

// Add the route




server.route({ // GET content 
    method: 'GET',
    path: '/student/{email}/{password}', //path'
    config: {
        auth: false,
        description: 'Get student by email and password',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('students').find().toArray(); //ชื่อตาราง
            var usr_data;

            for (let i = 0; i < res.length; i++) {
                if (res[i].email == req.params.email && res[i].password == req.params.password)
                    usr_data = res[i]; //เอาผลลัพธ์จากการเช็คเก็บใส่ตัวแปรชื่อ usr_data

            }

            return {
                data: usr_data,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});




server.route({ // GET content 
    method: 'GET',
    path: '/teacher/{email}/{password}', //path'
    config: {
        auth: false,
        description: 'Get teacher by email and password',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('teacher').find().toArray(); //ชื่อตาราง
            var usr_data;

            for (let i = 0; i < res.length; i++) {
                if (res[i].email == req.params.email && res[i].password == req.params.password)
                    usr_data = res[i]; //เอาผลลัพธ์จากการเช็คเก็บใส่ตัวแปรชื่อ usr_data

            }

            return {
                data: usr_data,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});
server.route({ // GET content 
    method: 'GET',
    path: '/subject', //path'
    config: {
        auth: false,
        description: 'Get all subject',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('subject').find().toArray(); //ชื่อตาราง

            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});

server.route({ // GET content 
    method: 'GET',
    path: '/codejoine', //path'
    config: {
        auth: false,
        description: 'Get student by email and password',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('code_Joine').find().toArray(); //ชื่อตาราง

            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});


server.route({
    method: 'POST',
    path: '/codejoine/add',
    config: {
        tags: ['api'],
        description: 'Add New user',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('code_Joine').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({ // GET content 
    method: 'GET',
    path: '/checkname', //path'
    config: {
        auth: false,
        description: 'Get student by email and password',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('checkname').find().toArray(); //ชื่อตาราง

            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});
server.route({ // GET content 
    method: 'GET',
    path: '/codecheck', //path'
    config: {
        auth: false,
        description: 'Get student by email and password',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('code_Check').find().toArray(); //ชื่อตาราง

            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});

server.route({
    method: 'POST',
    path: '/codecheck/add',
    config: {
        tags: ['api'],
        description: 'Add New user',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('code_Check').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});



server.route({
    method: 'POST',
    path: '/student/add',
    config: {
        tags: ['api'],
        description: 'Add New user',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('students').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});



server.route({
    method: 'POST',
    path: '/teacher/add',
    config: {
        tags: ['api'],
        description: 'Add New user',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('teacher').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});


server.route({
    method: 'POST',
    path: '/subject/add',
    config: {
        tags: ['api'],
        description: 'Add New subject',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('subject').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});
server.route({
    method: 'POST',
    path: '/checkname/add',
    config: {
        tags: ['api'],
        description: 'Add list student',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('checkname').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});
server.route({
    method: 'PUT',
    path: '/checknameofStu/{code}',
    config: {
        tags: ['api'],
        description: 'Update code in check name page',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('checkname').update({ "code": request.params.code }, { $push: { "student": request.payload } });
                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});
server.route({
    method: 'PUT',
    path: '/joinclasswithsubject/{code}',
    config: {
        tags: ['api'],
        description: 'Update code in check name page',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('subject').update({ "code": request.params.code }, { $push: { "student": request.payload } });
                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'PUT',
    path: '/clearCode/{code}',
    config: {
        tags: ['api'],
        description: 'Update code in check name page',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('checkname').update({ "code": request.params.code }, { $set: { "code": "" } });
                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/codecheck/delete/{code}',
    config: {
        tags: ['api'],
        description: 'Delete promotion by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('code_Check').remove({ code: request.params.code });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/checkname/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('checkname').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/student/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete promotion by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('students').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/teacher/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete promotion by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('teacher').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});



server.route({
    method: 'DELETE',
    path: '/subject/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete subject by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('subject').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});




// Start the server
const init = async() => {
    await server.register([{
            plugin: require('hapi-mongodb'),
            options: dbOpts
        },
        require('inert'),
        require('vision'),
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);
    await server.start();
    console.log(`Server started at ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(1);
})

init();